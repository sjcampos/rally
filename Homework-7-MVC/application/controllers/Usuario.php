<?php

class Usuario extends CI_Controller{

    function __contruct(){
        parent::__contruct();

        $this->load->model('Model_Usuario');
        
        
    }

    public function index(){
        $this->load->model('Model_Usuario');
        $data['contenido'] = 'usuario/index'; //Mandar plantilla a la vista index
        $data['selCliente'] = $this->Model_Usuario->SelectCliente();
        $this->load->view("plantilla", $data);

    }

    public function InsertCliente(){
        $datos = $this->input->post();
        $this->load->model('Model_Usuario');
        if(isset($datos))
        {
            $txtcedula = $datos['txtcedula'];
            $txtnombre = $datos['txtnombre'];
            $txtapellido = $datos['txtapellido'];
            $txttelefono = $datos['txttelefono'];
            $this->Model_Usuario->InsertarCliente($txtcedula,$txtnombre,$txtapellido,$txttelefono);
            redirect('');
        }
    }

    public function delete($cedula = NULL){
        $this->load->model('Model_Usuario');
        if($cedula != NULL){
            $this->Model_Usuario->DeleteCliente($cedula);
            redirect('');
        }

    }

    public function editar($cedula = NULL)
    {
        $this->load->model('Model_Usuario');
        if($cedula != NULL)
        {
            $data['contenido'] = 'usuario/editar';
            $data['datoscliente'] = $this->Model_Usuario->SelectById($cedula);
            $this->load->view("plantilla", $data);
        }else{
            redirect('');
        }
    }

    public function update()
    {
        $datos = $this->input->post();
        $this->load->model('Model_Usuario');
        if(isset($datos))
        {
            $txtcedula = $datos['txtcedula'];
            $txtnombre = $datos['txtnombre'];
            $txtapellido = $datos['txtapellido'];
            $txttelefono = $datos['txttelefono'];
            $this->Model_Usuario->UpdateCliente($txtcedula,$txtnombre,$txtapellido,$txttelefono);
            redirect('');
        }
    }



}