<?php

class Model_Usuario extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    public function SelectCliente(){
        $query  = $this->db->query("Select * from Cliente");

        return $query->result();

    }

    public function InsertarCliente($cedula,$nombre,$apellido,$telefono){

        $datos = array (
            'cedula' => $cedula,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'telefono' => $telefono

        );
        $this->db->insert('cliente',$datos);

    }


    public function DeleteCliente($cedula){
        $this->db->where('cedula', $cedula);
        $this->db->delete('cliente');
    }


    public function SelectById($cedula){
        $query  = $this->db->query("Select * from Cliente WHERE cedula = $cedula");

        return $query->result();

    }

    public function UpdateCliente($txtcedula,$txtnombre,$txtapellido,$txttelefono){
        $datosnuevos = array (
            'cedula' => $txtcedula,
            'nombre' => $txtnombre,
            'apellido' => $txtapellido,
            'telefono' => $txttelefono

        );
        $this->db->where('cedula', $txtcedula);
        $this->db->update('cliente', $datosnuevos);

    }


}