
<!---Aqui va a ir el crud---->

<h1>CRUD</h1>
<!-- Nav tabs -->
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Lista</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Registro</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
  

    <table class ="table table-striped">
        <thead>
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Telefono</th>
            <th>Opciones</th>
        </thead>
        <tbody>
            <?php foreach($selCliente as $value ) { ?>
                <tr>
                <td> <?php echo $value->cedula; ?></td>
                <td> <?php echo $value->nombre; ?></td>
                <td> <?php echo $value->apellido; ?></td>
                <td> <?php echo $value->telefono; ?></td>
                <td>
                    <a href="<?php echo base_url('usuario/delete/'.$value->cedula)?>">Eliminar</a>
                    <a href="<?php echo base_url('usuario/editar/'.$value->cedula)?>">Editar</a>
                </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
  
  
  
  </div>
  <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading">
              <h1 class="panel-title text-center">Registro</h1>
            </div>
            <div class="panel-body">
              <form  method="POST" action="<?php echo base_url('usuario/InsertCliente')?>" >
               <div class="form-group"  >
                <label for="cedula">Cedula</label>
                <input type="text" class="form-control" name="txtcedula" id="cedula" required placeholder="Cedula">
              </div> 
              <div class="form-group"  >
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" name="txtnombre" id="nombre" required placeholder="Nombre">
              </div> 
              <div class="form-group"  >
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" name="txtapellido" id="apellido" required placeholder="Apellido">
              </div>  
              <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" name="txttelefono" id="telefono"  required placeholder="Telefono">
              </div>
              <div class="form-group text-center">
                <input name="" id="" class="btn btn-dark" type="submit" value="Registro"  >
              </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  
  
  </div>
</div>
