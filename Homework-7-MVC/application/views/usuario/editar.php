
<div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading">
              <h1 class="panel-title text-center">Datos cliente</h1>
            </div>
            <div class="panel-body">
              <form  method="POST" action="<?php echo base_url('usuario/update')?>" >
              <?php foreach($datoscliente as $value ) { ?>
                
               <div class="form-group">
                <label for="cedula">Cedula</label>
                <input type="text" class="form-control" name="txtcedula" id="cedula" value="<?php echo $value->cedula; ?>">
              </div> 
              <div class="form-group"  >
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" name="txtnombre" id="nombre" value="<?php echo $value->nombre;?>">
              </div> 
              <div class="form-group"  >
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" name="txtapellido" id="apellido" value="<?php echo $value->apellido; ?>">
              </div>  
              <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" name="txttelefono" id="telefono" value="<?php echo $value->telefono; ?>" >
              </div>
              <div class="form-group text-center">
                <input name="" id="" class="btn btn-dark" type="submit" value="Guardar"  >
              </div> 
              <?php } ?>
              </form>
            </div>
          </div>
        </div>
      </div>
</div>
